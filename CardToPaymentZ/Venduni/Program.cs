﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics; // StackTrace
using CronNET; // CronDaemon
using ECR2ATLLib;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Newtonsoft.Json;
using System.Collections.Concurrent;

namespace Venduni
{

    public class Program
    {
        private Object ConsoleWriteLock = new Object();
        private Object DatabaseQueryLock = new Object();
        private Object ReconnectLock = new Object();
        private Object VoidListProcess = new Object();

        private Broker BrokerMgr;
        private Database DatabaseMgr;
        private CronDaemon CronMgr;
        private EventWaitHandle ProcessLock;

        // Ключ = pos_transaction.id   значение = pos_terminal.id
        private ConcurrentDictionary<UInt64, UInt32> VoidList = new ConcurrentDictionary<UInt64, UInt32>();

        public Program()
        {
            // Устанавливаем консольный заголовок
            Console.Title = "Venduni Card To Payment Z";
            // Создаём соеденение с БД
            DatabaseMgr = new Database {
                Parent = this,
                Hostname = Configuration.MySQL.Hostname,
                Port = Configuration.MySQL.Port,
                Username = Configuration.MySQL.User,
                Password = Configuration.MySQL.Password,
                Dataname = Configuration.MySQL.Database };
            DatabaseMgr.Survive();
            // Создаём соеденение с Брокером
            BrokerMgr = new Broker {
                Parent = this,
                SendPingPacket = Configuration.MQTT.SendPing,
                CompressJson = Configuration.MQTT.CompressJson,
                Hostname = Configuration.MQTT.Hostname,
                Port = Configuration.MQTT.Port,
                Username = Configuration.MQTT.User,
                Password = Configuration.MQTT.Password,
                Input = Configuration.MQTT.Request,
                Output = Configuration.MQTT.Response,
                AdminIO = Configuration.MQTT.AdminIO,
                OnMessage = OnBrokerMessage };
            BrokerMgr.Survive();
            // Создаём задачи для Cron и запускаем Cron
            CronMgr = new CronDaemon();
            CronMgr.AddJob("* * * * *", OnCheckConnections); // Каждую минуту проверяем соеденение
            CronMgr.AddJob("* * * * *", OnClosePayments); // Каждую минуту проверяем платежи
            CronMgr.AddJob("40 2 * * *", OnCloseBusinessDay); // Каждую ночь в 2:40 закрываем бизнесс день
            CronMgr.Start();
            // Создаём блокировщик основного потока + БЛОК основного потока
            ProcessLock = new EventWaitHandle(false, EventResetMode.ManualReset, Configuration.MainMutexName);
            ProcessLock.WaitOne();
        }

        public void OnInitialize()
        {
            
        }

        // Придерживает соеденения при жизни
        private void OnCheckConnections()
        {
            if ( Monitor.TryEnter(ReconnectLock)) {
                try {
                    DatabaseMgr.Survive();
                    BrokerMgr.Survive();
                }
                finally {
                    Monitor.Exit(ReconnectLock);
                }
            }
        }

        private void OnBrokerMessage(object sender, MqttMsgPublishEventArgs evt)
        {
            if (evt.Topic.Equals(Configuration.MQTT.Request)) {

                MQ_OutputClass output = new MQ_OutputClass();
                try {
                    MQ_InputClass input = JsonConvert.DeserializeObject<MQ_InputClass>(Encoding.UTF8.GetString(evt.Message));
                    output.guid = input.guid;
                    output.time = input.time;

                    String sql = $"SELECT pt.* FROM pos_terminal pt WHERE pt.id = @APosID LIMIT 1";
                    Dictionary<String, dynamic> para = new Dictionary<String, dynamic> { { "@APosID", input.guid } };
                    List<DBT_Terminal> term_raw = DatabaseMgr.Query<DBT_Terminal>(sql, para);

                    if (term_raw == null) {
                        output.status = "error";
                        output.message = "Unable to detect TerminalID in our database. Bad Id?";
                        BrokerMgr.Publish(output);
                        return;
                    }

                    UInt64 transaction_id = DatabaseMgr.CreatePaymentTransaction(input.guid, input.lang, input.amount);
                    if (transaction_id < 1) {
                        output.status = "error";
                        output.message = "Unable to generate new Transaction ID. System fault";
                        BrokerMgr.Publish(output);
                        throw new Exception("Unable to generate new Transaction ID. System fault");
                    }

                    output.status = "requested";
                    BrokerMgr.Publish(output);

                    UInt64 receipt_number = DatabaseMgr.GetReceiptNumber(transaction_id);
                    ECR2Connection cn = new ECR2Connection {
                        Host = term_raw[0].service_addr,
                        Port = term_raw[0].service_port,
                        NII = term_raw[0].nii,
                        Password = term_raw[0].passwd,
                        Language = "en",
                        WaitDialogVisible = (Configuration.ShowEcrDialog ? 1 : 0)
                    };

                    ECR2InitiateCommand it = new ECR2InitiateCommand { Connection = cn };
                    it.LanguageSelect(input.lang.ToLower());
                    DatabaseMgr.UpdatePaymentTransactionStatus(transaction_id, "requested", it.ErrorCode, it.ErrorMessage, it.Data);
                    Thread.Sleep(100);

                    ECR2Transaction tr = new ECR2Transaction { Connection = cn };
                    String amount = input.amount.ToString("F");
                    tr.Purchase(receipt_number.ToString(), amount);

                    LogMessage($"Pos: {term_raw[0].id}, Reciept: {receipt_number}, Amount: {amount} Result: {tr.ErrorCode}", "PUR");

                    // LogMessage("PURCHASE")

                    switch (tr.ErrorCode[0]) {
                        case 'P':
                            if (tr.ErrorCode.Equals("P000")) {
                                output.status = "success";
                            } else {
                                output.status = "error";
                                VoidList.TryAdd(transaction_id, term_raw[0].id);
                            }
                            break;
                        case 'B':
                            output.status = "declined";
                            break;
                        case 'E':
                        default:
                            output.status = "error";
                            VoidList.TryAdd(transaction_id, term_raw[0].id);
                            break;
                    }

                    DatabaseMgr.UpdatePaymentTransactionStatus(transaction_id, output.status, tr.ErrorCode, tr.ErrorMessage, tr.Receipt);
                    BrokerMgr.Publish(output);
                }
                catch (Exception err) {
                    LogError(err);
                }
            }
            else if ( evt.Topic.Equals(Configuration.MQTT.AdminIO)) {
                MQ_AdminClass output = new MQ_AdminClass { way = "output" };
                try {
                    MQ_AdminClass input = JsonConvert.DeserializeObject<MQ_AdminClass>(Encoding.UTF8.GetString(evt.Message));
                    if (input.way.Equals("output")) return; // Ignore output packets (Own packets)
                    if (input.way.Equals("input")) {
                        switch (input.action) {
                            case "closeday":
                                OnCloseBusinessDay();
                                break;
                            case "void":
                                // Check pos_transaction_id & pos_terminal_id existance
                                if (( input.pos_transaction_id is null ) || (input.pos_transaction_id.Length < 1 ) || (input.pos_terminal_id is null) || (input.pos_terminal_id.Length < 1 )) {
                                    output.message = "Unsupported pos_transaction_id and/or pos_terminal_id {pos_transaction_id:???,pos_terminal_id:???}";
                                    break;
                                }
                                UInt64 pos_transaction_id = 0; UInt64.TryParse(input.pos_transaction_id, out pos_transaction_id);
                                UInt32 pos_terminal_id = 0; UInt32.TryParse(input.pos_terminal_id, out pos_terminal_id);
                                VoidList.TryAdd(pos_transaction_id, pos_terminal_id);
                                OnClosePayments();
                                break;
                            case "status":
                                break;
                            default:
                                output.message = "Unsupported action {action:???}";
                                break;
                        }
                    } else {
                        output.message = "Unsupported {way:???}";
                    }
                    BrokerMgr.Publish(output);
                }
                catch ( Exception err ) {
                    LogError(err);
                }

            }
        }

        private void OnClosePayments()
        {
            if (Monitor.TryEnter(VoidListProcess)) {
                try {
                    new Thread(() => {
                        try {
                            // Ключ = pos_transaction.id   значение = pos_terminal.id
                            foreach (KeyValuePair<UInt64, UInt32> void_rec in VoidList) {
                                UInt64 transaction_id = void_rec.Key;
                                UInt32 terminal_id = void_rec.Value;

                                String sql;
                                Dictionary<String, dynamic> para;

                                sql = "SELECT pt.* FROM pos_terminal pt WHERE pt.id = @APosID LIMIT 1";
                                para = new Dictionary<String, dynamic> { { "@APosID", terminal_id } };
                                List<DBT_Terminal> terminal_rows = DatabaseMgr.Query<DBT_Terminal>(sql, para);

                                sql = "SELECT pt.* FROM pos_transaction pt WHERE pt.id = @ATransactionID LIMIT 1";
                                para = new Dictionary<String, dynamic> { { "@ATransactionID", terminal_id } };
                                List<DBT_Transaction> transaction_rows = DatabaseMgr.Query<DBT_Transaction>(sql, para);

                                if ((terminal_rows == null) || (transaction_rows == null)) {
                                    throw new Exception("terminal_rows or transaction_rows is NULL CHECK DB!");
                                }

                                UInt64 pos_void_id = DatabaseMgr.CreateVoidTransaction(transaction_id);

                                ECR2Connection cn = new ECR2Connection {
                                    Host = terminal_rows[0].service_addr,
                                    Port = terminal_rows[0].service_port,
                                    NII = terminal_rows[0].nii,
                                    Password = terminal_rows[0].passwd,
                                    Language = "en",
                                    WaitDialogVisible = (Configuration.ShowEcrDialog ? 1 : 0)
                                };

                                ECR2InitiateCommand cm = new ECR2InitiateCommand { Connection = cn };
                                cm.LanguageSelect("en");
                                Thread.Sleep(100);

                                ECR2Transaction tr = new ECR2Transaction { Connection = cn };
                                tr.Void(transaction_rows[0].receipt_number.ToString(), null);

                                DatabaseMgr.UpdateVoidTransaction(pos_void_id, tr.ErrorCode, tr.ErrorCode, tr.ErrorMessage);

                                if (( tr.ErrorCode == "P000" ) || ( tr.ErrorCode == "B014" )) {
                                    VoidList.TryRemove(void_rec.Key, out UInt32 dummy);
                                }

                            }
                        }
                        finally {
                            Monitor.Exit(VoidListProcess);
                        }
                    });
                }
                catch (Exception err) {
                    LogError(err);
                    Monitor.Exit(VoidListProcess);
                }
            }
        }

        private void OnCloseBusinessDay()
        {
            this.LogMessage("Closing Business Day");

            String SQL = @"SELECT * FROM pos_terminal WHERE active = 1";
            List<DBT_Terminal> pure_list = DatabaseMgr.Query<DBT_Terminal>(SQL, null);
            foreach (DBT_Terminal row in pure_list) {

                UInt64 transaction_id = DatabaseMgr.CreateBusinessdayTransaction(row.id);
                UInt64 receipt_number = DatabaseMgr.GetReceiptNumber(transaction_id);

                ECR2Connection cn = new ECR2Connection {
                    Host = row.service_addr,
                    Port = row.service_port,
                    NII = row.nii,
                    Password = row.passwd,
                    Language = "en",
                    WaitDialogVisible = (Configuration.ShowEcrDialog ? 1 : 0) };

                ECR2InitiateCommand cm = new ECR2InitiateCommand { Connection = cn };
                cm.LanguageSelect(cn.Language);
                Thread.Sleep(100);

                ECR2Transaction tr = new ECR2Transaction { Connection = cn };
                tr.SettlementForAllHost(transaction_id.ToString());
                Thread.Sleep(100);

                DatabaseMgr.UpdateBusinessdayTransaction(transaction_id, tr.ErrorCode, tr.ErrorMessage, tr.Receipt);
                this.LogMessage($" + clse Pos: {row.id} Result: {tr.ErrorCode}");

                if ( tr.ErrorCode == "P000" ) {
                    cm.TerminalInit();
                    Thread.Sleep(100);
                    this.LogMessage($" + init Pos: {row.id} Result: {tr.ErrorCode}");
                }

            }
            this.LogMessage("Complete");
        }

        public void LogMessage(String message, String prefix=null)
        {
            if (prefix == null) {
                prefix = "MSG";
            } else {
                prefix = prefix.PadLeft(3, '_').ToUpper().Substring(0, 3);
            }
            StringBuilder lines = new StringBuilder();
            lines.Append(DateTime.Now.ToString("yyyyMMdd-HHmmss "));
            lines.Append(prefix + ": " + message);
            lock (ConsoleWriteLock) {
                Console.WriteLine(lines.ToString());
            }
        }

        public void LogError(Exception err)
        {
            StringBuilder lines = new StringBuilder();
            lines.Append(DateTime.Now.ToString("yyyyMMdd-HHmmss "));

            StackTrace trace = new StackTrace(err, true);
            String className = trace.GetFrame(0).GetMethod().DeclaringType.FullName;
            lines.Append("EXC " + className + ": " + err.Message + Environment.NewLine);
            lines.Append(err.StackTrace);

            lock (ConsoleWriteLock) {
                Console.Error.WriteLine(lines.ToString());
            }
        }


    }
}
