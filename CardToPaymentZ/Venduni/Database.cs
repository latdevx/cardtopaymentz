﻿using System;
using System.Reflection;
using System.Text; // StringBuilder
using System.Threading; // Thread.Sleep
using System.Collections.Generic; // List<T>
using MySql.Data.MySqlClient; // MySqlConnection
using System.Data;


namespace Venduni
{

#pragma warning disable IDE1006 // disable Naming rule violation

    internal class DBT_Terminal // @SQL: TABLE pos_terminal
    {
        public UInt32 id { get; set; }
        public String service_addr { get; set; }
        public UInt16 service_port { get; set; }
        public String nii { get; set; }
        public String passwd { get; set; }
        public String host_number { get; set; }
        public String mac_addr { get; set; }
        public String serial_keypad { get; set; }
        public String serial_magstripe { get; set; }
        public String serial_contactless { get; set; }
    }

    internal class DBT_Transaction // @SQL: TABLE pos_transaction
    {
        public UInt64 id { get; set; }
        public UInt32 pos_terminal_id { get; set; }
        public UInt64 receipt_number { get; set; }
        public String response_code { get; set; }
        public String response_message { get; set; }
        public String response_receipt { get; set; }
    }

    internal class DBT_Payment // @SQL: TABLE pos_payment
    {
        public UInt64 pos_transaction_id { get; set; }
        public String request_status { get; set; }
        public String lang { get; set; }
        public DateTime ctime { get; set; }
        public DateTime mtime { get; set; }
        public Double amount { get; set; }
    }

    internal class DBT_BusinessDay // @SQL: TABLE pos_businessday
    {
        public UInt64 pos_transaction_id { get; set; }
        public DateTime ctime { get; set; }
        public DateTime mtime { get; set; }
    }

#pragma warning restore IDE1006


    internal class Database
    {
        public Program Parent;
        public String Hostname;
        public UInt16 Port;
        public String Username;
        public String Password;
        public String Dataname;

        private MySqlConnection Connection = new MySqlConnection();
        private Object DatabaseQueryLock;

        public Database()
        {
            DatabaseQueryLock = new Object();
        }

        public void Survive()
        {
            try {
                lock (DatabaseQueryLock) {
                    Connection.Ping();
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
            }
            while (Connection.State != ConnectionState.Open) {
                try {
                    Parent.LogMessage("Connect mysql://" + Username + "@" + Hostname + ":" + Port.ToString() + "/" + Dataname);
                    Connection.ConnectionString = (new StringBuilder())
                        .Append("server=" + Hostname)
                        .Append(";port = " + Port.ToString())
                        .Append(";user=" + Username)
                        .Append(";password=" + Password)
                        .Append(";database=" + Dataname).ToString();
                    Connection.Open();
                    Parent.LogMessage("Status Connected");
                }
                catch (MySqlException err) {
                    Parent.LogMessage("Status: Not Connected");
                    Parent.LogError(err);
                    Thread.Sleep(5000);
                    continue;
                }
            }

        }

        // @SQL: CREATE FUNCTION TerminalExists(ATerminalID int UNSIGNED) RETURNS tinyint
        public Boolean TerminalExists(UInt32 ATerminalID)
        {
            try {
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand("TerminalExists", Connection) { CommandType = CommandType.StoredProcedure }) {
                        com.Parameters.AddWithValue("ATerminalID", ATerminalID);
                        com.Parameters.Add(new MySqlParameter("Result", MySqlDbType.UInt64) { Direction = ParameterDirection.ReturnValue });
                        com.ExecuteScalar();
                        Object Result = com.Parameters["Result"].Value;
                        if (Result is DBNull) {
                            return false;
                        }
                        else {
                            return 0 != (Byte) Result;
                        }
                    }
                }
            } catch ( MySqlException err) {
                Parent.LogError(err);
                return false;
            }
        }


        // @SQL: CREATE FUNCTION CreatePaymentTransaction (ATerminalID int UNSIGNED, ALang char(2), AAmount float(11,4)) RETURNS bigint UNSIGNED
        public UInt64 CreatePaymentTransaction(UInt32 ATerminalID, String ALang, Double AAmount)
        {
            try {
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand("CreatePaymentTransaction", Connection) { CommandType = CommandType.StoredProcedure }) {
                        com.Parameters.AddWithValue("ATerminalID", ATerminalID);
                        com.Parameters.AddWithValue("ALang", ALang);
                        com.Parameters.AddWithValue("AAmount", AAmount);
                        com.Parameters.Add(new MySqlParameter("Result", MySqlDbType.UInt64) { Direction = ParameterDirection.ReturnValue });
                        com.ExecuteScalar();
                        Object Result = com.Parameters["Result"].Value;
                        if ( Result is DBNull) {
                            return 0;
                        }
                        else {
                            return (UInt64)Result;
                        }
                    }
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
                return 0;
            }
        }

        // @SQL: CREATE FUNCTION CreateBusinessdayTransaction(ATerminalID int UNSIGNED) RETURNS bigint UNSIGNED
        public UInt64 CreateBusinessdayTransaction(UInt32 ATerminalID)
        {
            try {
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand("CreateBusinessdayTransaction", Connection) { CommandType = CommandType.StoredProcedure }) {
                        com.Parameters.AddWithValue("ATerminalID", ATerminalID);
                        com.Parameters.Add(new MySqlParameter("Result", MySqlDbType.UInt64) { Direction = ParameterDirection.ReturnValue });
                        com.ExecuteScalar();
                        Object Result = com.Parameters["Result"].Value;
                        if (Result is DBNull) {
                            return 0;
                        }
                        else {
                            return (UInt64)Result;
                        }
                    }
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
                return 0;
            }
        }

        // @SQL: CREATE FUNCTION CreateVoidTransaction(APosTransactionID bigint UNSIGNED) RETURNS bigint UNSIGNED
        public UInt64 CreateVoidTransaction(UInt64 APosTransactionID)
        {
            try {
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand("CreateVoidTransaction", Connection) { CommandType = CommandType.StoredProcedure }) {
                        com.Parameters.AddWithValue("APosTransactionID", APosTransactionID);
                        com.Parameters.Add(new MySqlParameter("Result", MySqlDbType.UInt64) { Direction = ParameterDirection.ReturnValue });
                        com.ExecuteScalar();
                        Object Result = com.Parameters["Result"].Value;
                        if (Result is DBNull) {
                            return 0;
                        }
                        else {
                            return (UInt64)Result;
                        }

                    }
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
                return 0;
            }
        }

        // @SQL: CREATE FUNCTION GetReceiptNumber(ATransactionId bigint UNSIGNED) RETURNS bigint UNSIGNED
        public UInt64 GetReceiptNumber(UInt64 ATransactionId)
        {
            try {
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand("GetReceiptNumber", Connection) { CommandType = CommandType.StoredProcedure }) {
                        com.Parameters.AddWithValue("ATransactionId", ATransactionId);
                        com.Parameters.Add(new MySqlParameter("Result", MySqlDbType.UInt64) { Direction = ParameterDirection.ReturnValue });
                        com.ExecuteScalar();
                        Object Result = com.Parameters["Result"].Value;
                        if (Result is DBNull) {
                            return 0;
                        }
                        else {
                            return (UInt64)Result;
                        }

                    }
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
                return 0;
            }
        }

        // @SQL: CREATE PROCEDURE UpdatePaymentTransactionStatus(ATransactionId bigint UNSIGNED, ANewStatus varchar(20), ACode varchar(10), AMessage text, AReceipt text)
        public void UpdatePaymentTransactionStatus(UInt64 ATransactionId, String ANewStatus, String ACode, String AMessage, String AReceipt)
        {
            try {
                String[] Possible_ANewStatus = { "requested", "declined", "passed", "error" };
                if ( Array.IndexOf(Possible_ANewStatus, ANewStatus) < 0 ) {
                    throw new Exception("Parameter ANewStatus is enum of (requested,declined,passed,error)");
                }
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand("UpdatePaymentTransactionStatus", Connection) { CommandType = CommandType.StoredProcedure }) {
                        com.Parameters.AddWithValue("ATransactionId", ATransactionId);
                        com.Parameters.AddWithValue("ANewStatus", ANewStatus);
                        com.Parameters.AddWithValue("ACode", ACode);
                        com.Parameters.AddWithValue("AMessage", AMessage);
                        com.Parameters.AddWithValue("AReceipt", AReceipt);
                        com.ExecuteScalar();
                    }
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
            }
        }

        // @SQL: CREATE PROCEDURE UpdateBusinessdayTransaction(ATransactionId bigint UNSIGNED, ACode varchar(10), AMessage text, AReceipt text)
        public void UpdateBusinessdayTransaction(UInt64 ATransactionId, String ACode, String AMessage, String AReceipt)
        {
            try {
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand("UpdateBusinessdayTransaction", Connection) { CommandType = CommandType.StoredProcedure }) {
                        com.Parameters.AddWithValue("ATransactionId", ATransactionId);
                        com.Parameters.AddWithValue("ACode", ACode);
                        com.Parameters.AddWithValue("AMessage", AMessage);
                        com.Parameters.AddWithValue("AReceipt", AReceipt);
                        com.ExecuteScalar();
                    }
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
            }
        }

        // @SQL: CREATE PROCEDURE UpdateVoidTransaction(APosVoidID bigint UNSIGNED, ACode varchar(10), AMessage text, AReceipt text)
        public void UpdateVoidTransaction(UInt64 APosVoidID, String ACode, String AMessage, String AReceipt)
        {
            try {
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand("UpdateVoidTransaction", Connection) { CommandType = CommandType.StoredProcedure }) {
                        com.Parameters.AddWithValue("APosVoidID", APosVoidID);
                        com.Parameters.AddWithValue("ACode", ACode);
                        com.Parameters.AddWithValue("AMessage", AMessage);
                        com.Parameters.AddWithValue("AReceipt", AReceipt);
                        com.ExecuteScalar();
                    }
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
            }
        }


        public List<T> Query<T>(String Query, IDictionary<String, dynamic> values = null) where T : new()
        {
            try {
                lock (DatabaseQueryLock) {
                    using (MySqlCommand com = new MySqlCommand { Connection = this.Connection }) {
                        // SQL запрос
                        com.CommandText = Query;
                        // Аргументы если есть
                        if (values != null) {
                            foreach (var value in values) {
                                com.Parameters.AddWithValue(value.Key, value.Value);
                            }
                        }
                        // Запускаем чтение 
                        using (MySqlDataReader statement = com.ExecuteReader()) {
                            if (!statement.HasRows) return null;
                            List<T> result = new List<T>();
                            while (statement.Read()) {
                                T row = new T();
                                for (UInt16 i = 0; i < statement.FieldCount; i++) {
                                    PropertyInfo pi = typeof(T).GetProperty(statement.GetName(i));
                                    if (pi == null) continue;
                                    if (statement.GetValue(i) is DBNull) {
                                        pi.SetValue(row, null, null);
                                    }
                                    else {
                                        pi.SetValue(row, statement.GetValue(i), null);
                                    }
                                }
                                result.Add(row);
                            }
                            return result;
                        }
                    }
                }
            }
            catch (MySqlException err) {
                Parent.LogError(err);
            }
            return null;
        }

    }
}
