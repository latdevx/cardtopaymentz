﻿using System;
using System.Text; // Encoding
using System.Threading; // Thread.Sleep
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using Newtonsoft.Json;

namespace Venduni
{

#pragma warning disable IDE1006,CS0649 // disable Naming rule violation

    internal class MQ_AdminClass
    {
        public String packet = "status";
        public String way = "out";
        public String action;
        public String status;
        public String message;
        public String pos_transaction_id;
        public String pos_terminal_id;
    }

    internal class MQ_InputClass
    {
        public UInt32 guid;
        public String time;
        public String lang;
        public Double amount;
    }

    internal class MQ_OutputClass
    {
        public String packet = "payment";
        public UInt32 guid;
        public UInt64 receipt_no;
        public String time;
        public String status;
        public String message;
    }

#pragma warning restore IDE1006,CS0649

    internal class Broker
    {

        // Это основная коннфигурация, должна быть принята до запуска класса
        public Program Parent;
        public Boolean SendPingPacket;
        public Boolean CompressJson;
        public String Hostname;
        public UInt16 Port;
        public String Username;
        public String Password;
        public String Input;
        public String Output;
        public String AdminIO;
        public MqttClient.MqttMsgPublishEventHandler OnMessage;

        private MqttClient Connection;
        private Object MqttResponseLock;
        private JsonSerializerSettings JSerializerSetup;

        public Broker()
        {
            MqttResponseLock = new Object();
            JSerializerSetup = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
        }

        public void Survive()
        {
            if ((Connection != null) && SendPingPacket) {
                Publish(new {
                    packet = "ping",
                    timestamp = DateTime.Now.ToString("yyyyMMddHHmmss")
                });
            }

            while ((Connection == null) || (!Connection.IsConnected)) {
                try {
                    Parent.LogMessage("Connecting mqtt://" + Username + '@' + Hostname + ':' + Port.ToString());
                    Connection = new MqttClient(Hostname);
                    Connection.MqttMsgPublishReceived += OnMessage;
                    Connection.Connect(Username, Username, Password);
                    Connection.Subscribe(new String[] { Input, AdminIO }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
                    Publish(new {
                        packet = "init",
                        timestamp = DateTime.Now.ToString("yyyyMMddHHmmss")
                    });
                    Parent.LogMessage("Status: Connected");
                }
                catch (Exception err) {
                    Parent.LogMessage("Status: Not Connected");
                    Parent.LogError(err);
                    Thread.Sleep(5000);
                    continue;
                }
            }
        }

        public void Publish(byte[] message)
        {
            lock (MqttResponseLock) {
                Connection.Publish(Output, message);
            }
        }

        public void Publish(String message)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(message);
            Publish(bytes);
        }

        public void Publish(Object message)
        {
            Formatting JFormatting = CompressJson ? Formatting.None : Formatting.Indented;
            String Json = JsonConvert.SerializeObject(message, JFormatting, JSerializerSetup);
            byte[] JsonBytes = Encoding.UTF8.GetBytes(Json);
            Publish(JsonBytes);
        }

    }
}
