-- Drop everything ordered by foreign key
DROP TABLE IF EXISTS pos_businessday; -- pos_transaction
DROP TABLE IF EXISTS pos_payment; -- pos_transaction
DROP TABLE IF EXISTS pos_void; -- pos_transaction
DROP TABLE IF EXISTS pos_transaction; -- pos_terminal
DROP TABLE IF EXISTS pos_terminal;

CREATE TABLE pos_terminal (
  id int UNSIGNED NOT NULL AUTO_INCREMENT,
  active tinyint NOT NULL DEFAULT 0 COMMENT 'Is this active=~0 terminal or not=0',
  service_addr varchar(32) NOT NULL COMMENT 'SELF-SERVICE application address',
  service_port smallint UNSIGNED NOT NULL COMMENT 'SELF-SERVICE application port',
  nii varchar(30) NOT NULL COMMENT 'Sets NII',
  passwd varchar(30) NOT NULL COMMENT 'Merchant password',
  host_number varchar(30) NOT NULL COMMENT 'Terminal ID without check digit',
  mac_addr char(17) NOT NULL COMMENT 'Terminal MAC address',
  serial_keypad varchar(128) DEFAULT NULL COMMENT 'Keypads serial number',
  serial_magstripe varchar(128) DEFAULT NULL COMMENT 'Magnetstripe reader serial number',
  serial_contactless varchar(128) DEFAULT NULL COMMENT 'Contactless serial number',
  PRIMARY KEY(id)
) ENGINE=INNODB COLLATE utf8_general_ci
COMMENT 'POS Terminals table information';


CREATE TABLE pos_transaction (
  id bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  pos_terminal_id int UNSIGNED NOT NULL COMMENT 'Link to pos_terminal.id',
  receipt_number bigint UNSIGNED NOT NULL COMMENT 'Receipt number incremental for each POS',
  response_code varchar(10) DEFAULT NULL COMMENT 'ECR Code',
  response_message text DEFAULT NULL COMMENT 'ECR Message',
  response_receipt text DEFAULT NULL COMMENT 'ECR Receipt',
  PRIMARY KEY(id),
  CONSTRAINT `FK_pos_transaction.pos_terminal_id` FOREIGN KEY (pos_terminal_id)
    REFERENCES pos_terminal(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=INNODB COLLATE utf8_general_ci
COMMENT 'POS Transactions table, required to hold every transaction and transaction results';


CREATE TABLE pos_payment (
  pos_transaction_id bigint UNSIGNED NOT NULL COMMENT 'Link to pos_transaction.id',
  request_status enum('requested', 'declined', 'passed', 'error') NOT NULL DEFAULT 'requested' COMMENT 'Current request status',
	lang enum('lv','ru','en') NOT NULL COMMENT 'Client language',
	ctime datetime NOT NULL COMMENT 'Payment creation date',
  mtime datetime NULL DEFAULT NULL COMMENT 'Payment last update date',
  amount float(11,4) NOT NULL DEFAULT '0.0000' COMMENT 'Amount of cash to subtract from client',
  PRIMARY KEY (pos_transaction_id),
  INDEX `IX_pos_payment.request_status` (request_status),
  CONSTRAINT `FK_pos_payment.pos_transaction_id` FOREIGN KEY(pos_transaction_id)
    REFERENCES pos_transaction(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB COLLATE=utf8_general_ci
COMMENT 'POS Payment transactions list';


CREATE TABLE pos_businessday (
  pos_transaction_id bigint UNSIGNED NOT NULL COMMENT 'Link to pos_transaction.id',
	ctime datetime NOT NULL COMMENT 'Payment creation date',
  mtime datetime NULL DEFAULT NULL COMMENT 'Payment last update date',
  PRIMARY KEY (pos_transaction_id),
  CONSTRAINT `FK_pos_businessday.pos_transaction_id` FOREIGN KEY(pos_transaction_id)
  REFERENCES pos_transaction(id) ON UPDATE CASCADE ON DELETE CASCADE
)  ENGINE=InnoDB COLLATE=utf8_general_ci
COMMENT 'POS Close business day transactions list';


CREATE TABLE `pos_void` (
  id bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  pos_transaction_id bigint UNSIGNED NOT NULL,
  void_response_code varchar(10) DEFAULT NULL COMMENT 'ECR Code',
  void_response_message text DEFAULT NULL COMMENT 'ECR Message',
  void_response_receipt text DEFAULT NULL COMMENT 'ECR Receipt',
  PRIMARY KEY (id),
  CONSTRAINT `FK_pos_void.pos_transaction_id` FOREIGN KEY(pos_transaction_id)
    REFERENCES pos_transaction(id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE=InnoDB COLLATE=utf8_general_ci
COMMENT 'Uncompleted and error transactions list';


-- -------------                       ------------- --
-- --------     FUNTIONS AND PROCEDURES     -------- --
-- -------------                       ------------- --

DELIMITER $$

  DROP FUNCTION IF EXISTS TerminalExists;
  CREATE FUNCTION TerminalExists(ATerminalID int UNSIGNED) RETURNS tinyint
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Check is terminal exists ~0 or not exists = 0'
  BEGIN
    DECLARE terminal_exists tinyint UNSIGNED;
    SELECT count(1) INTO terminal_exists FROM pos_terminal pt WHERE pt.active != 0 AND pt.id = ATerminalID LIMIT 1;
    RETURN terminal_exists;
  END$$


  DROP FUNCTION IF EXISTS CreateNewTransaction;
  CREATE FUNCTION CreateNewTransaction(ATerminalID int UNSIGNED) RETURNS bigint UNSIGNED
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Creates new untyped transaction and results pos_transaction.id or 0 on error'
  BEGIN
    DECLARE terminal_exists tinyint UNSIGNED;
    DECLARE max_receipt_number bigint UNSIGNED;    
    SELECT count(1) INTO terminal_exists FROM pos_terminal pt WHERE pt.active != 0 AND pt.id = ATerminalID LIMIT 1;
    IF ( terminal_exists = 1 ) THEN
      SELECT coalesce(max(pt.receipt_number), 0) INTO max_receipt_number
        FROM pos_transaction pt WHERE pt.pos_terminal_id = ATerminalID;
      INSERT INTO pos_transaction ( pos_terminal_id, receipt_number, response_code, response_message, response_receipt)
        VALUES ( ATerminalID, max_receipt_number + 1, NULL, NULL, NULL);
      RETURN last_insert_id();
    ELSE
      RETURN 0;
    END IF;  
  END$$


  DROP FUNCTION IF EXISTS CreatePaymentTransaction$$
  CREATE FUNCTION CreatePaymentTransaction (ATerminalID int UNSIGNED, ALang char(2), AAmount float(11,4)) RETURNS bigint UNSIGNED
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Creates new payment transaction and results pos_transaction.id or 0 on error'
  BEGIN
    DECLARE new_transaction_id bigint UNSIGNED;
    SET new_transaction_id = CreateNewTransaction(ATerminalID);
    IF ( new_transaction_id != 0 ) THEN
      INSERT INTO pos_payment (pos_transaction_id, request_status, lang, ctime, mtime, amount)
        VALUES (new_transaction_id, 'requested', ALang, NOW(), NULL, AAmount);
    END IF;
    RETURN new_transaction_id;
  END$$


  DROP FUNCTION  IF EXISTS CreateBusinessdayTransaction$$
  CREATE FUNCTION CreateBusinessdayTransaction(ATerminalID int UNSIGNED) RETURNS bigint UNSIGNED
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Creates new Close Business Day transaction and results pos_transaction.id or 0 on error'
  BEGIN
    DECLARE new_transaction_id bigint UNSIGNED;
    SET new_transaction_id = CreateNewTransaction(ATerminalID);
    IF ( new_transaction_id != 0 ) THEN
      INSERT INTO pos_businessday(pos_transaction_id, ctime)
        VALUES (new_transaction_id, NOW());
    END IF;
    RETURN new_transaction_id;
  END$$

  DROP FUNCTION IF EXISTS CreateVoidTransaction$$
  CREATE FUNCTION CreateVoidTransaction(APosTransactionID bigint UNSIGNED) RETURNS bigint UNSIGNED
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Creates new Void transaction'
  BEGIN
    DECLARE transaction_exists tinyint UNSIGNED;
    SELECT COUNT(1) INTO transaction_exists FROM pos_transaction pt WHERE pt.id = APosTransactionID LIMIT 1;
    IF ( transaction_exists = 1 ) THEN
      INSERT INTO pos_void (pos_terminal_id) VALUES (APosTransactionID);
      RETURN last_insert_id();
    ELSE
      RETURN 0;
    END IF;
  END$$


  DROP FUNCTION IF EXISTS GetReceiptNumber$$
  CREATE FUNCTION GetReceiptNumber(ATransactionId bigint UNSIGNED) RETURNS bigint UNSIGNED
    DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Get receipt number using transaction id'
  BEGIN
    DECLARE receipt_number bigint UNSIGNED;
    SELECT pt.receipt_number INTO receipt_number
      FROM pos_transaction pt WHERE pt.id = ATransactionId LIMIT 1;
    RETURN coalesce(receipt_number, 0);
  END$$


  DROP PROCEDURE IF EXISTS UpdatePaymentTransactionStatus$$
  CREATE PROCEDURE UpdatePaymentTransactionStatus(ATransactionId bigint UNSIGNED, ANewStatus varchar(20), ACode varchar(10), AMessage text, AReceipt text)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Update Payment transaction status'
  BEGIN
    DECLARE transaction_exists tinyint UNSIGNED;
    SELECT count(1) INTO transaction_exists FROM pos_payment pp WHERE pp.pos_transaction_id = ATransactionId LIMIT 1;
    IF (transaction_exists = 1) THEN
      UPDATE pos_transaction pt SET pt.response_code = ACode, pt.response_message = AMessage, pt.response_receipt = AReceipt WHERE id = ATransactionId LIMIT 1;
      UPDATE pos_payment pp SET pp.request_status = ANewStatus, pp.mtime = now() WHERE pp.pos_transaction_id = ATransactionId LIMIT 1;
    END IF;
  END$$


  DROP PROCEDURE IF EXISTS UpdateBusinessdayTransaction$$
  CREATE PROCEDURE UpdateBusinessdayTransaction(ATransactionId bigint UNSIGNED, ACode varchar(10), AMessage text, AReceipt text)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Update Close Business Day transaction status'
  BEGIN
    DECLARE transaction_exists tinyint UNSIGNED;
    SELECT count(1) INTO transaction_exists FROM pos_businessday pb WHERE pb.pos_transaction_id = ATransactionId LIMIT 1;
    IF (transaction_exists = 1) THEN
      UPDATE pos_transaction pt SET pt.response_code = ACode, pt.response_message = AMessage, pt.response_receipt = AReceipt WHERE id = ATransactionId LIMIT 1;
      UPDATE pos_businessday pb SET pb.mtime = now() WHERE pb.pos_transaction_id = ATransactionId LIMIT 1;
    END IF;
  END$$

  DROP PROCEDURE IF EXISTS UpdateVoidTransaction$$
  CREATE PROCEDURE UpdateVoidTransaction(APosVoidID bigint UNSIGNED, ACode varchar(10), AMessage text, AReceipt text)
    NOT DETERMINISTIC
    CONTAINS SQL
    SQL SECURITY INVOKER
    COMMENT 'Update Void transaction status'
  BEGIN
    DECLARE transaction_exists tinyint UNSIGNED;
    SELECT COUNT(1) INTO transaction_exists FROM pos_void pv WHERE pv.id = APosVoidID LIMIT 1;
    IF (transaction_exists = 1) THEN
      UPDATE pos_void pvu set pvu.void_response_code = ACode, pvu.void_response_message = AMessage, pvu.void_response_receipt = AReceipt LIMIT 1;
    END IF;
  END$$


DELIMITER ;

-- -------------                  ------------- --
-- --------     SQL TABLE CONTENTS     -------- --
-- -------------                  ------------- --



INSERT INTO pos_terminal (active, service_addr, service_port, nii, passwd, host_number, mac_addr, serial_keypad, serial_magstripe, serial_contactless)
VALUES (1, '127.0.0.1', '60008', '0003', '000000', '123456', '54-7F-54-D9-0E-6C',  '15031UN20052985', '13255UN80014775', NULL);
INSERT INTO pos_terminal (active, service_addr, service_port, nii, passwd, host_number, mac_addr, serial_keypad, serial_magstripe, serial_contactless)
VALUES (1, '127.0.0.1', '11', '9999', '123456', '123456', 'DU-MM-Y_-PO-SS-!!',  '00000DUMMYPOS', '0000DUMMYPOS', NULL);

